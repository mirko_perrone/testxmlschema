﻿using System;
using Gtk;
using System.Windows.Forms;
namespace testXmlSchema
{
	public partial class StartWindow : Gtk.Window
	{
		public StartWindow () :
			base (Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}
		private static void startDialogNContatto(){
			NuovoUtenteDialog win = new NuovoUtenteDialog ();
			win.Show ();
		}
		protected void OnNewActionActivated (object sender, EventArgs e)
		{
			startDialogNContatto ();
		}
		protected void OnAboutActionActivated (object sender, EventArgs e)
		{
			DialogCustomMsg m = new DialogCustomMsg ("\n Ringraziamenti a \n Straw Hat \n Dr.Virus \n Angelo :D \n  ");
		}
		protected void OnFindAction1Activated (object sender, EventArgs e)
		{
			DialogCustomFinder m = new DialogCustomFinder (textviewRubrica,"Nome",labelNumero);
		}
		protected void OnNomeActionActivated (object sender, EventArgs e)
		{
			DialogCustomFinder m = new DialogCustomFinder (textviewRubrica,"Nome",labelNumero);
		}
		protected void OnCognomeActionActivated (object sender, EventArgs e)
		{
			DialogCustomFinder m = new DialogCustomFinder (textviewRubrica,"Cognome",labelNumero);
		}
		protected void OnIndirizzoActionActivated (object sender, EventArgs e)
		{
			DialogCustomFinder m = new DialogCustomFinder (textviewRubrica,"Indirizzo",labelNumero);
		}
		protected void OnNumeroTelefonoActionActivated (object sender, EventArgs e)
		{
			DialogCustomFinder m = new DialogCustomFinder (textviewRubrica,"Telefono",labelNumero);
		}
		protected void OnIDActionActivated (object sender, EventArgs e)
		{
			DialogCustomFinder m = new DialogCustomFinder (textviewRubrica,"ID",labelNumero);
		}
		protected void OnQuitActionActivated (object sender, EventArgs e)
		{
			Gtk.Application.Quit ();
		}
	}
}

