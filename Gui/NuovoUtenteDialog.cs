﻿using System;
using System.Windows.Forms;

using Gtk;

namespace testXmlSchema
{
	public partial class NuovoUtenteDialog : Gtk.Dialog
	{
		public NuovoUtenteDialog ()
		{
			this.Build ();
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			String msg;
			MetodiXml m = new MetodiXml ();
			Persona p = new Persona (null,entryNome.Text.ToString(),
				entryCognome.Text.ToString(),
			entryIndirizzo.Text.ToString(),
			entryNumero.Text.ToString());
			try{
			m.ScriviPersonaXml (p);
			}catch(Exception){
				msg= "Errore Riprovare";
				DialogCustomMsg d = new DialogCustomMsg(msg);
				d.Show();
			}finally{
				this.Hide ();
			}
		}

		protected void OnButtonCancelClicked (object sender, EventArgs e)
		{
			this.Hide();
		}
	}
}

