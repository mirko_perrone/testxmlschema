﻿using System;

namespace testXmlSchema
{
	public partial class DialogCustomMsg : Gtk.Dialog
	{
		/// <summary>
		/// Data in imput una stringa restituisce una dialog con il messaggio in imput come label.
		/// Da usare al posto della MessageBox buggata e poro dio.
		/// </summary>
		/// <param name="msg">Message.</param>
		public DialogCustomMsg (string msg)
		{
			this.Build ();
			this.MessaggioLbl.Text = msg;
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			this.Hide ();
		}
	}
}

