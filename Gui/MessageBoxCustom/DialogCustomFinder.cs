﻿using System;
using Gtk;
using System.Collections.Generic;


namespace testXmlSchema
{
	public partial class DialogCustomFinder : Gtk.Dialog
	{
		String _msg;
		String _cerca;
		TextView _textviewRubrica;
		Label _labelNumero;
		public DialogCustomFinder ()
		{
			this.Build ();
		}
		public DialogCustomFinder (TextView textviewRubrica ,string msg, Label labelNumero)
		{	
			this.Build ();
			this.label1.Text = msg;	
			this._textviewRubrica = textviewRubrica;
			this._msg = msg;
			this._labelNumero = labelNumero;
		}
		protected void OnButtonOkClicked (object sender, EventArgs e)
		{	_cerca = entry1.Text;
			ResultScrolledWindow (_textviewRubrica,_cerca);
			this.Hide ();
		}
		private void ResultScrolledWindow(TextView textviewRubrica,String cerca){
			MetodiXml m = new MetodiXml ();
			String _cerca = cerca;
			String _cosaCercare = _msg;

			var p = new List<Persona>();
			Persona persona = new Persona ();
			p = m.CercaContattoGuiArrayTest (_cerca, persona, p, _cosaCercare);
			String testo = null;
			int i = 0;
			foreach (Persona personaInLista in p)
			{
				 testo=testo+"\nNuovo utente\n\nNome: " + personaInLista.Nome + "\n" +
						"Cognome: " + personaInLista.Cognome + "\n" +
						"Indirizzo: " +personaInLista.Indirizzo + "\n" +
					"Numero di telefono: " + personaInLista.NumeroTelefono + "\n";
				i++;
			}
			_labelNumero.Text = i.ToString ();
			textviewRubrica.Buffer.Text=testo;
			//return _textviewRubrica;
		}
		protected void OnButtonCancelClicked (object sender, EventArgs e)
		{
			this.Hide ();
		}
		protected void OnEntry1TextInserted (object o, TextInsertedArgs args)
		{
            _cerca = entry1.Text.ToLower();
			ResultScrolledWindow (_textviewRubrica, _cerca);
		}

		protected void OnEntry1EditingDone (object sender, EventArgs e)
		{
            _cerca = entry1.Text.ToLower();
			ResultScrolledWindow (_textviewRubrica, _cerca);
		}

		protected void OnEntry1Backspace (object sender, EventArgs e)
		{
            _labelNumero.Text="0";
			_textviewRubrica.Buffer.Text = "";
		}
	}
}

