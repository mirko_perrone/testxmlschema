﻿using System;

namespace testXmlSchema
{
	public partial class AutenticazioneUtente : Gtk.Dialog
	{
		String _nome_utente="Admin";
		String _passwd_utente="Admin";
		public AutenticazioneUtente ()
		{
			this.Build ();
		}


		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
			if (_nome_utente == entryNomeUtente.Text.ToString () && _passwd_utente == entryPasswd.Text.ToString ()) {
				StartWindow win = new StartWindow ();
				this.Hide ();
				win.Show ();
			} else {
				DialogCustomMsg m = new DialogCustomMsg ("Nome utente o password errati.");
			}
		}

		protected void OnButtonCancelClicked (object sender, EventArgs e)
		{
			this.Hide ();
		}
	}
}

