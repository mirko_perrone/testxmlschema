﻿using System;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using Gtk;

namespace testXmlSchema
{
	class MainClass{
		public static void Main(string[] args){
			MetodiXml m = new MetodiXml ();
			m.ControlloEsistenzaXml ();
			startWindow ();
			//test2();
		}
		/// <summary>
		/// Just start the main window
		/// </summary>
		private static void startWindow(){
			Application.Init ();
			AutenticazioneUtente m = new AutenticazioneUtente ();
			m.Show ();
			Application.Run ();
		}
		/// <summary>
		/// metodo di test.da usare al posto del main.
		/// </summary>
		private static void test(){
			//			m.CercaContattoDaID ();
			//			m.CercaContattoDaNome ();
			//			m.CercaContattoDaCognome ();
			//			m.CercaContattoDaIndirizzo ();
			//			m.CercaContattoDalNumero ();
			MetodiXml m = new MetodiXml ();
			XmlDocument xmlDocument = new XmlDocument ();
			xmlDocument.Load ("Rubrica.xml");

			//XDocument xDocument = m.DocumentToXDocument(xmlDocument);
			XDocument xDocument = m.ToXDocument (xmlDocument);

			IEnumerable<XElement> elements = xDocument.Element ("Rubrica").Elements ("Contatto");
			foreach (XElement element in elements) {
				Console.WriteLine ("Source element: {0} : value = {1}", element.Name, element.Value);
			}
			foreach (XElement element in elements.Elements()){
				Console.WriteLine ("Child element: {0}", element);
			}
		}	
		private static void test2(){
			
			Console.WriteLine ("Scegli cosa cercare tra le varie opzioni:\n" +
				"0 per ID \n" +
				"1 per Nome \n" +
				"2 per Cognome \n" +
				"3 per Indirizzo \n" +
				"4 per Numero Di telefono.");
			int i = Int16.Parse(Console.ReadLine ());
			String DoveCercare;
			MetodiXml m = new MetodiXml ();

			switch (i) {
			case 0:
				DoveCercare = "ID";
				m.CercaContatto (DoveCercare);
				break;
			case 1:
				DoveCercare = "Nome";
				m.CercaContatto (DoveCercare);
				break;
			case 2:
				DoveCercare = "Cognome";
				m.CercaContatto (DoveCercare);
				break;
			case 3:
				DoveCercare = "Indirizzo";
				m.CercaContatto (DoveCercare);
				break;
			case 4:
				DoveCercare = "Telefono";
				m.CercaContatto (DoveCercare);
				break;
			}
		}
	}  
}
