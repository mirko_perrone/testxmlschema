﻿using System;
using System.Collections.Generic;

namespace testXmlSchema
{
	public class Persona
	{
		public string ID { get; set; }
		public string Nome { get; set; }
		public string Cognome  { get; set; }
		public string Indirizzo  { get; set; }
		public string NumeroTelefono  { get; set; }

		public Persona ()
		{
		}
		public Persona (string ID,string Nome,string Cognome,string Indirizzo,string NumeroTelefono)
		{
			this.ID = ID;
			this.Nome = Nome;
			this.Cognome = Cognome;
			this.Indirizzo = Indirizzo;
			this.NumeroTelefono = NumeroTelefono;							
		}
		public Persona (string Nome,string Cognome,string Indirizzo,string NumeroTelefono)
		{
			this.Nome = Nome;
			this.Cognome = Cognome;
			this.Indirizzo = Indirizzo;
			this.NumeroTelefono = NumeroTelefono;							
		}/// <summary>
		/// Arraies the di persona.
		/// Prende in imput gli arrey costitutivi della persona e crea per ognuno di essi 
		/// un oggetto persona restituendo un una lista chiave valore di persone...
		/// (Non è stato possibile creare un array di persone[] perchè ci ho provato 
		/// ma restituivaa errori e la soluzione migliore è stata di optare per una lista)
		/// </summary>
		/// <param name="arrNome">Arr nome.</param>
		/// <param name="arrCognome">Arr cognome.</param>
		/// <param name="arrIndirizzo">Arr indirizzo.</param>
		/// <param name="arrTelefono">Arr telefono.</param>
		public List<Persona> ListaDiPersone(String[] arrNome,String[] arrCognome,String[] arrIndirizzo,String[] arrTelefono){
			List<Persona> listP = new List<Persona> ();
			for(int i=0;i< arrNome.Length;i++) {
				Persona p = new Persona(); 
				p.Nome = arrNome [i];
				p.Cognome = arrCognome [i];
				p.Indirizzo= arrIndirizzo [i];
				p.NumeroTelefono = arrTelefono [i];
				listP.Add (p);
			}
			return listP;
		}


	}
}

