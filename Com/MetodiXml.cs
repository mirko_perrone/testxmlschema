﻿using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Windows.Forms;
using Gtk;
using System.Collections.Generic;


namespace testXmlSchema
{
	public class MetodiXml
	{	String msg;
		public MetodiXml (){
		}
		///Esegue l'immissione di un nuovo contatto command line
		public void CreaContatto(){
			string Nome ;
			string Cognome ;
			string Indirizzo;
			string NumeroTelefono ;
			int numero = 0;
			System.Console.WriteLine ("Quanti contatti desideri aggiungere ? ");
			try{
				numero=Int32.Parse( Console.ReadLine());
			}
			catch(Exception )
			{
				Console.WriteLine ("Errore.Non hai inserito un numero.Ripovare.");

			}

			for (int i=0; i<numero;i++){
				System.Console.WriteLine ("Inserisci il nome : ");
				Nome = Console.ReadLine ();

				System.Console.WriteLine ("Inserisci il cognome : ");
				Cognome = Console.ReadLine ();

				System.Console.WriteLine ("Inserisci l' indirizzo : ");
				Indirizzo = Console.ReadLine ();

				System.Console.WriteLine ("Inserisci il numero di telefono : ");
				NumeroTelefono = Console.ReadLine ();

				Persona p = new Persona (null,Nome, Cognome, Indirizzo, NumeroTelefono);
				ScriviPersonaXml (p);
			}
		}

		/// <summary>
		///	Metodo che controlla se esiste o meno il file Rubrica.xml
		/// eventualmente lo crea e restituisce il numero di contatti :D serve per creare un id incrementale.
		/// </summary>
		/// <returns>The esistenza xml.</returns>

		public string ControlloEsistenzaXml(){
			string curFile = @"Rubrica.xml";

			if (File.Exists(curFile)){
				//Console.WriteLine("File Rubrica.xml esistente.");
			}
			else{
				Console.WriteLine("File Rubrica.xml non esistente .");
				ScriviXml ();
				Console.WriteLine("File Rubrica.xml Creato .");
			}
			XmlDocument doc = new XmlDocument();
			doc.Load("Rubrica.xml");

			// Get and display all the book titles.
			XmlElement root = doc.DocumentElement;
			XmlNodeList elemList = root.GetElementsByTagName("Contatto");
			return (elemList.Count).ToString();//non metto +1 perchè parto da 0 e non da 1 :D
			//Console.WriteLine(File.Exists(curFile) ? "File exists." : ScriviXml());
		}

		///Metodo che controlla l'esistenza di un contatto.
		///Fa la ricerca per numero di telefono
		///Questo algoritmo può essere implementato in un altro metodo per fare la ricerca
		public bool ControlloContattoEsistenteXml(Persona p){
			ControlloEsistenzaXml ();
			XmlDocument doc = new XmlDocument();
			doc.Load("Rubrica.xml");
			//Fa la ricerca per numero di telefono
			// Get and display all the Contatto element.
			XmlElement root = doc.DocumentElement;
			XmlNodeList elemList = root.GetElementsByTagName("Telefono");
			//Display all content from nodes
			foreach (XmlNode Telefono in elemList)
			{
				if (Telefono.InnerText.Equals (p.NumeroTelefono)) {
					msg= "Contatto esistente";
					DialogCustomMsg d = new DialogCustomMsg(msg);
					d.Show();
					//Console.WriteLine ("Contatto esistente.");
					return true;//Per far funzionare il controllo settare a false
				}

			} 

			return true;
		}	

		/// <summary>
		/// Cercas the contatto inserire il numero i
		/// i = 1 cerca per nome
		/// i = 2 cerca per cognome
		/// i = 3 cerca per indirizzo
		/// i = 4 cerca per Numero di telefono 
		/// </summary>
	
		public void CercaContatto(String DoveCercare){
			//TO-DO risolvere eventuali ridondanze.
			Persona p = new Persona ();
			String Numero;

			Console.WriteLine("\nInerisci "+DoveCercare+" per la ricerca: ");
			Numero=Console.ReadLine ();
			XPathDocument doc = new XPathDocument("Rubrica.xml");
			XPathNavigator nav = doc.CreateNavigator();

			// Compile a standard XPath expression
			XPathExpression expr;

			try{
				for (int i = 0; i <= 4; i++) {
					XPathNodeIterator iterator = null;
					switch (i) {
					case 1:
						expr = nav.Compile ("/Rubrica/Contatto["+DoveCercare+"='" + Numero + "']/Nome");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.Nome = nav2.Value;

						}
						break;
					case 2:
						expr = nav.Compile ("/Rubrica/Contatto["+DoveCercare+"='" + Numero + "']/Cognome");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.Cognome = nav2.Value;

						}
						break;
					case 3:
						expr = nav.Compile ("/Rubrica/Contatto["+DoveCercare+"='" + Numero + "']/Indirizzo");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.Indirizzo = nav2.Value;

						}
						break;
					case 4:
						expr = nav.Compile ("/Rubrica/Contatto["+DoveCercare+"='" + Numero + "']/Telefono");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.NumeroTelefono = nav2.Value;

						}
						break;
					}
				}

				Console.WriteLine ("Nome : "+p.Nome);
				Console.WriteLine ("Cognome : "+p.Cognome);
				Console.WriteLine ("Indirizzo : "+p.Indirizzo);
				Console.WriteLine ("Numero di telefono : "+p.NumeroTelefono);

			}catch(Exception){
				msg= "Errore Riprovare";
				DialogCustomMsg d = new DialogCustomMsg(msg);
				d.Show();
			}

		}
		/// <summary>
		/// Cercas the contatto da nome per Gui.
		/// </summary>

		public Persona CercaContattoGui(String cerca,Persona p,String CosaCercare ){
			//TO-DO risolvere eventuali ridondanze.
			//Persona p = new Persona ();
			String Numero=cerca;
			XPathDocument doc = new XPathDocument("Rubrica.xml");
			XPathNavigator nav = doc.CreateNavigator();

			// Compile a standard XPath expression
			XPathExpression expr;
			try{
				for (int i = 0; i <= 4; i++) {
					XPathNodeIterator iterator = null;
					switch (i) {
					case 1:
						expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Nome");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.Nome = nav2.Value;
						}
						break;
					case 2:
						expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Cognome");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.Cognome = nav2.Value;

						}
						break;
					case 3:
						expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Indirizzo");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.Indirizzo = nav2.Value;

						}
						break;
					case 4:
						expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Telefono");
						iterator = nav.Select (expr);
						while (iterator.MoveNext ()) {
							XPathNavigator nav2 = iterator.Current.Clone ();
							p.NumeroTelefono = nav2.Value;

						}
						break;
					}
				}

//				Console.WriteLine ("Nome : "+p.Nome);
//				Console.WriteLine ("Cognome : "+p.Cognome);
//				Console.WriteLine ("Indirizzo : "+p.Indirizzo);
//				Console.WriteLine ("Numero di telefono : "+p.NumeroTelefono);

				return p;
			}catch(Exception){
				msg= "Errore Riprovare";
				DialogCustomMsg d = new DialogCustomMsg(msg);
				d.Show();
				return p;
			}
		}
		/// <summary>
		/// Cercas the contatto da nome per Gui.
		/// </summary>

		public List<Persona> CercaContattoGuiArrayTest(String cerca,Persona persona,List<Persona> listaP,String CosaCercare ){
			//TO-DO risolvere eventuali ridondanze.
			//Persona p = new Persona ();

			String Numero=cerca;
			XPathDocument doc = new XPathDocument("Rubrica.xml");
			XPathNavigator nav = doc.CreateNavigator();

			// Compile a standard XPath expression
			XPathExpression expr;
			try{
				int j=0;
				//Definisco un array per ogni attributo dell'oggetto persona e lo riempo
				XPathNodeIterator iterator = null;
				expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Nome");
				iterator = nav.Select (expr);
				String[] arrNome= new string[iterator.Count];
				String[] arrCognome= new string[iterator.Count];
				String[] arrTelefono= new string[iterator.Count];
				String[] arrIndirizzo= new string[iterator.Count];
//				String[] arrNome= new string[];
//				String[] arrCognome= new string[];
//				String[] arrTelefono= new string[];
//				String[] arrIndirizzo= new string[];
				for (int i = 0; i <= 4; i++) {
					iterator = null;
					switch (i) {
						case 1:
							j=0;
							expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Nome");
							iterator = nav.Select (expr);

							while (iterator.MoveNext ()) {
								XPathNavigator nav2 = iterator.Current.Clone ();
								arrNome[j]= nav2.Value;
								j++;
							}
							break;
						case 2:
							j=0;
							expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Cognome");
							iterator = nav.Select (expr);

							while (iterator.MoveNext ()) {
								XPathNavigator nav2 = iterator.Current.Clone ();
								arrCognome[j]=nav2.Value;
								j++;
							}
							break;
						case 3:
							expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Indirizzo");
							iterator = nav.Select (expr);

							j=0;
							while (iterator.MoveNext ()) {
								XPathNavigator nav2 = iterator.Current.Clone ();
								arrIndirizzo[j] = nav2.Value;
								j++;

							}
							break;
						case 4:
							expr = nav.Compile ("/Rubrica/Contatto["+CosaCercare+"='" + Numero + "']/Telefono");
							iterator = nav.Select (expr);
							j=0;
							while (iterator.MoveNext ()) {
								XPathNavigator nav2 = iterator.Current.Clone ();
								arrTelefono[j] = nav2.Value;
								j++;

							}
							break;
						}
					listaP=persona.ListaDiPersone(arrNome,arrCognome,arrIndirizzo,arrTelefono);
				}
					

				//				Console.WriteLine ("Nome : "+p.Nome);
				//				Console.WriteLine ("Cognome : "+p.Cognome);
				//				Console.WriteLine ("Indirizzo : "+p.Indirizzo);
				//				Console.WriteLine ("Numero di telefono : "+p.NumeroTelefono);

				return listaP;
			}catch(Exception){
				msg= "Errore Riprovare";
				DialogCustomMsg d = new DialogCustomMsg(msg);
				d.Show();
				return listaP;
			}
		}
		/// <summary>
		///Metodo che data una persona in imput la aggiunge ad un file Rubrica.xml come contatto.
		/// </summary>
		/// <param name="p">P.</param>
		public void ScriviPersonaXml(Persona p){
			if (ControlloContattoEsistenteXml (p)) {
				string ID = ControlloEsistenzaXml ();

				XmlDocument doc = new XmlDocument ();
				doc.Load ("Rubrica.xml");

				XmlNode root = doc.DocumentElement;

				XmlElement eltPerson = doc.CreateElement ("Contatto");
				root.AppendChild (eltPerson);

				XmlElement eltID = doc.CreateElement ("ID");
                eltID.AppendChild (doc.CreateTextNode (ID.ToLower()));
				eltPerson.AppendChild (eltID);

				XmlElement eltNome = doc.CreateElement ("Nome");
                eltNome.AppendChild (doc.CreateTextNode (p.Nome.ToLower()));
				eltPerson.AppendChild (eltNome);

				XmlElement eltCognome = doc.CreateElement ("Cognome");
                eltCognome.AppendChild (doc.CreateTextNode (p.Cognome.ToLower()));
				eltPerson.AppendChild (eltCognome);

				XmlElement eltIndirizzo = doc.CreateElement ("Indirizzo");
                eltIndirizzo.AppendChild (doc.CreateTextNode (p.Indirizzo.ToLower()));
				eltPerson.AppendChild (eltIndirizzo);


				XmlElement eltNumeroCell = doc.CreateElement ("Telefono");
                eltNumeroCell.AppendChild (doc.CreateTextNode (p.NumeroTelefono.ToLower()));
				eltPerson.AppendChild (eltNumeroCell);

				FileStream file = new FileStream ("Rubrica.xml", FileMode.Create, FileAccess.Write);

				doc.Save (file);
				file.Close ();
				msg= "Tutto ok";
				DialogCustomMsg d = new DialogCustomMsg(msg);
				d.Show();
				//Console.WriteLine ("Contatto aggiunto con successo.");
			} else {
				msg= "Errore Riprovare";
				DialogCustomMsg d = new DialogCustomMsg(msg);
				d.Show();
			//Console.WriteLine ("Contatto non aggiunto con successo.");
			}
			//Console.WriteLine( doc.OuterXml );
		}
	
		/// <summary>
		/// Write the default xml.
		/// TO-DO implementare eventuale primo contatto con il propio nome e cognome,al posto di lucifero stella del mattino.
		/// </summary>
		public void ScriviXml(){
			XmlDocument doc = new XmlDocument();

			XmlDeclaration primaRiga = doc.CreateXmlDeclaration ("1.0","UTF-8",null);
			doc.AppendChild (primaRiga);

			XmlElement root = doc.CreateElement( "Rubrica" );
			doc.AppendChild( root );

			XmlElement eltPerson = doc.CreateElement( "Contatto" );
			root.AppendChild( eltPerson );

			XmlElement eltID = doc.CreateElement( "ID" );
			eltID.AppendChild( doc.CreateTextNode( "0" ) );
			eltPerson.AppendChild( eltID );

			XmlElement eltNome = doc.CreateElement( "Nome" );
			eltNome.AppendChild( doc.CreateTextNode( "lucifer" ) );
			eltPerson.AppendChild( eltNome );

			XmlElement eltCognome = doc.CreateElement( "Cognome" );
			eltCognome.AppendChild( doc.CreateTextNode(  "morningstar" ) );
			eltPerson.AppendChild( eltCognome );

			XmlElement eltIndirizzo = doc.CreateElement( "Indirizzo" );
			eltIndirizzo.AppendChild( doc.CreateTextNode(  "via dell'inferno 23" ) );
			eltPerson.AppendChild( eltIndirizzo );


			XmlElement eltNumeroCell = doc.CreateElement( "Telefono" );
			eltNumeroCell.AppendChild( doc.CreateTextNode(  "666999666" ) );
			eltPerson.AppendChild( eltNumeroCell );

			FileStream file = new FileStream ("Rubrica.xml", FileMode.Create, FileAccess.ReadWrite);
			XmlTextWriter xwriter = new XmlTextWriter(file, new UTF8Encoding());
			xwriter.Formatting = Formatting.Indented;
			//Console.WriteLine( doc.OuterXml );
			doc.Save(file);
			file.Close ();
		}

		/// <summary>
		/// Convert xDocument to XmlDocument
		/// </summary>
		/// <returns>The xml document.</returns>
		/// <param name="xDocument">X document.</param>
		public XmlDocument ToXmlDocument(XDocument xDocument){
			var xmlDocument = new XmlDocument();
			using(var xmlReader = xDocument.CreateReader()){
				xmlDocument.Load(xmlReader);
			}
			return xmlDocument;
		}
		/// <summary>
		/// Tos the X document.
		/// </summary>
		/// <returns>The X document.</returns>
		/// <param name="xmlDocument">Xml document.</param>
		public XDocument ToXDocument(XmlDocument xmlDocument){
			using (var nodeReader = new XmlNodeReader (xmlDocument)){
				nodeReader.MoveToContent ();
				return XDocument.Load (nodeReader);
			}
		}
		/// <summary>
		/// Documents to X document.
		/// </summary>
		/// <returns>The to X document.</returns>
		/// <param name="doc">Document.</param>
		public XDocument DocumentToXDocument(XmlDocument doc){
			return XDocument.Parse(doc.OuterXml);
		}
		/// <summary>
		/// Documents to X document navigator.
		/// </summary>
		/// <returns>The to X document navigator.</returns>
		/// <param name="doc">Document.</param>
		public XDocument DocumentToXDocumentNavigator(XmlDocument doc){
			return XDocument.Load(doc.CreateNavigator().ReadSubtree());
		}
		/// <summary>
		/// Documents to X document reader.
		/// </summary>
		/// <returns>The to X document reader.</returns>
		/// <param name="doc">Document.</param>
		public XDocument DocumentToXDocumentReader(XmlDocument doc){
			return XDocument.Load(new XmlNodeReader(doc));
		}
	}
 
  }


